package com.example.trafficlightsystem.state

class TrafficLight {

    enum class State {
        RED, ORANGE, GREEN
    }

    var state: State? = null
        private set

    private val states: ArrayList<State> = ArrayList()
    private var itr: ListIterator<State>

    init {
        states.add(State.GREEN)
        states.add(State.ORANGE)
        states.add(State.RED)
        itr = states.listIterator()
    }

    /**
     * Reset the iterator to the beginning,
     * and loop through until the iterator matches the state
     * @param state - The state to set the iterator to
     */
    fun setState(state: State) {
        itr = states.listIterator()
        while (itr.hasNext()) {
            val tempState = itr.next()
            if (tempState == state) {
                this.state = tempState
                break
            }
        }
    }

}