package com.example.trafficlightsystem.ui

import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.AnimationSet
import android.widget.ImageView
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import com.example.trafficlightsystem.R
import com.example.trafficlightsystem.databinding.FragmentSecondScreenBinding
import com.example.trafficlightsystem.state.TrafficLight
import kotlinx.coroutines.*


class SecondScreen : Fragment() {
    private var _binding: FragmentSecondScreenBinding? = null
    private val binding get() = _binding!!

    companion object {
        const val RED_DELAY: Long = 4000L
        const val GREEN_DELAY: Long = 4000L
        const val ORANGE_DELAY: Long = 1000L
    }

    private lateinit var redLight: ImageView
    private lateinit var orangeLight: ImageView
    private lateinit var greenLight: ImageView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSecondScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

  /*  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val carName = arguments?.let { SecondScreenArgs.fromBundle(it).carArgument }
        binding.carNameTxt.text = "Car model: $carName"

        redLight = binding.redCircleImg
        orangeLight = binding.orangeCircleImg
        greenLight = binding.greenCircleImg

        val switchLight = binding.switchLight

        switchLight.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    switchLight.text = switchLight.textOn
                    switchLight.showText

           GlobalScope.launch(Dispatchers.Main) {
                        makeLightActive(greenLight, R.color.green)
                        animateImage(greenLight, GREEN_DELAY)
                        delay(GREEN_DELAY)

                        makeLightActive(orangeLight, R.color.orange)
                        animateImage(orangeLight, ORANGE_DELAY)
                        delay(ORANGE_DELAY)

                        makeLightActive(redLight, R.color.red)
                        animateImage(redLight, RED_DELAY)
                        delay(RED_DELAY)
                    }
                } else {
                    switchLight.text = switchLight.textOff
                    switchLight.showText
                    disableTrafficLights()
                }
            }
      }*/

    private fun makeLightActive(img: ImageView, color: Int) =
        img.drawable.setColorFilter(
            ResourcesCompat.getColor(
                resources,
                color,
                null
            ), PorterDuff.Mode.ADD
        )

    fun makeLightInactive(img: ImageView) =
        img.drawable.setColorFilter(
            ResourcesCompat.getColor(
                resources,
                R.color.inactive_light,
                null
            ), PorterDuff.Mode.ADD
        )

    private fun disableTrafficLights() {
        makeLightInactive(redLight)
        makeLightInactive(orangeLight)
        makeLightInactive(greenLight)
    }

    private fun animateImage(img: ImageView, delay: Long) {
        val fadeIn = AlphaAnimation(0f, 1f)
        val fadeOut = AlphaAnimation(1f, 0f)
        val set = AnimationSet(false)

        set.addAnimation(fadeIn)
        set.addAnimation(fadeOut)
        set.duration = delay
        img.startAnimation(set)

        set.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {}
            override fun onAnimationRepeat(animation: Animation) {
            }
            override fun onAnimationEnd(animation: Animation) {
                makeLightInactive(img)
            }
        })
    }

    //use this function to make the traffic light repeat until turned off
    private fun CoroutineScope.launchPeriodicAsync(
        repeatMillis: Long,
        action: () -> Unit
    ) = this.async {
        if (repeatMillis > 0) {
            while (isActive) {
                action()
                delay(repeatMillis)
            }
        } else {
            action()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val carName = arguments?.let { SecondScreenArgs.fromBundle(it).carArgument }
        binding.carNameTxt.text = "Car model: $carName"

        redLight = binding.redCircleImg
        orangeLight = binding.orangeCircleImg
        greenLight = binding.greenCircleImg

        val trafficLight = TrafficLight()
        val switchLight = binding.switchLight

        GlobalScope.launch(Dispatchers.Main) {

            var jobGreen = CoroutineScope(Dispatchers.IO).launchPeriodicAsync(GREEN_DELAY) {
                makeLightActive(greenLight, R.color.green)
                animateImage(
                    greenLight,
                    GREEN_DELAY
                )
            }
            var jobOrange = CoroutineScope(Dispatchers.IO).launchPeriodicAsync(ORANGE_DELAY) {
                makeLightActive(orangeLight, R.color.orange)
                animateImage(
                    orangeLight,
                    ORANGE_DELAY
                )
            }
            var jobRed = CoroutineScope(Dispatchers.IO).launchPeriodicAsync(RED_DELAY) {
                makeLightActive(redLight, R.color.red)
                animateImage(
                    redLight,
                    RED_DELAY
                )
            }

            switchLight.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    switchLight.text = switchLight.textOn
                    switchLight.showText
                    jobGreen.start()
                    jobOrange.start()
                    jobRed.start()

                    /* GlobalScope.launch(Dispatchers.Main) {
                    trafficLight.setState(TrafficLight.State.GREEN)
                    changeLight(trafficLight.state)
                    delay(GREEN_DELAY)
                    fadeOutImage(greenLight, GREEN_DELAY)

                    trafficLight.setState(TrafficLight.State.ORANGE)
                    changeLight(trafficLight.state)
                    delay(ORANGE_DELAY)
                    fadeOutImage(orangeLight, ORANGE_DELAY)

                    trafficLight.setState(TrafficLight.State.RED)
                    changeLight(trafficLight.state)
                    delay(RED_DELAY)
                    fadeOutImage(redLight, RED_DELAY)*/

//                }
                } else {
                    switchLight.text = switchLight.textOff
                    switchLight.showText
                    disableTrafficLights()
                    jobGreen.cancel()
                    jobOrange.cancel()
                    jobRed.cancel()

                }
            }
        }
    }
    private fun changeLight(state: TrafficLight.State?) {
        when (state) {
            TrafficLight.State.RED -> {
                //If the traffic light state is red,
                // make the green and orange lights inactive, and the red light active
                makeLightInactive(greenLight)
                makeLightInactive(orangeLight)
                makeLightActive(redLight, R.color.red)
            }
            TrafficLight.State.ORANGE -> {
                //If the traffic light state is orange,
                // make the green and red lights inactive, and the orange light active
                makeLightInactive(greenLight)
                makeLightInactive(redLight)
                makeLightActive(orangeLight, R.color.orange)
            }
            TrafficLight.State.GREEN -> {
                //If the traffic light state is green,
                // make the red and orange lights inactive, and the green light active
                makeLightInactive(redLight)
                makeLightInactive(orangeLight)
                makeLightActive(greenLight, R.color.green)
            }
            else -> Log.e("SecondScreen", "Invalid Traffic Light State")
        }
    }

    fun fadeOutImage(img: ImageView, delay: Long) {
        val fadeOut = AlphaAnimation(1F, 0F)
        fadeOut.interpolator = AccelerateInterpolator()

        Handler().postDelayed({
            img.drawable.setColorFilter(
                ResourcesCompat.getColor(
                    resources,
                    R.color.inactive_light,
                    null
                ), PorterDuff.Mode.ADD
            )
        }, 500)
        img.startAnimation(fadeOut)
    }

}